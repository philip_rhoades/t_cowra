---
layout: post
---

I have contacted the following organisations to let them know I have put them on the list:

* Building related:
	* Jamieson's Joinery and Building Centre
	* Corey Craig Building Services
	* 3 other builders
* Apsara Café
* Cowra Subway
* Cowra Laundry

I have emailed the following organisations to let them know that the list exists and organisations can be added to it for free:

* Visit Cowra
* Cowra Information and Neighbourhood Centre
* Cowra Guardian
* Cowra Business Hub
* Cowra Business Chamber


